# Abstract

\input knuth \par

# Executive Summary

\input knuth \par

# Contents

\input knuth \par

# References

\placepublications[criterium=text] \par

# Bibliography

\placepublications[criterium=all] \par

# Appendix
